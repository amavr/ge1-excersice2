/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mavrelos.eap.plh24.ge1.askhsh2;

/**
 *
 * @author mavrelos
 */
public class Game {

    private Team teamHome; //γηπεδούχος ομάδα
    private Team teamGuest; //φιλοξενούμενη ομάδα
    private int scoreHome; //σκορ γηπεδούχου ομάδας
    private int scoreGuest; //σκορ φιλοξενούμενης ομάδας   
    
    /** Will be overriden by children */
    int winnerPoints = 0;
    int loserPoints = 0;
    int nilPoints = 0;

    //constructor με τον οποίο εισάγονται οι δύο ομάδες
    //και το επιμέρους αποτέλεσμα
    public Game(Team teamHome, Team teamGuest, int scoreHome, int scoreGuest, int winnerPoints, int loserPoints, int nilPoints) {
        this.teamHome = teamHome;
        this.teamGuest = teamGuest;
        this.scoreHome = scoreHome;
        this.scoreGuest = scoreGuest;
        
        this.winnerPoints = winnerPoints;
        this.loserPoints = loserPoints;
        this.nilPoints = nilPoints;
    }
    public Game(){
        
    }

    public void setTeamHome(Team teamHome) {
        this.teamHome = teamHome;
    }

    public Team getTeamHome() {
        return teamHome;
    }

    public void setTeamGuest(Team teamGuest) {
        this.teamGuest = teamGuest;
    }

    public Team getTeamGuest() {
        return teamGuest;
    }

    public void setScoreHome(int scoreHome) {
        if (scoreHome >= 0) {
            this.scoreHome = scoreHome;
        } else {
            System.out.println("Score must be >=0");
        }
        //No Exception Handling
    }

    public int getScoreHome() {
        return scoreHome;
    }

    public void setScoreGuest(int scoreGuest) {
        if (scoreGuest >= 0) {
            this.scoreGuest = scoreGuest;
        } else {
            System.out.println("Score must be >=0");
        }
    }

    public int getScoreGuest() {
        return scoreGuest;
    }

    public int getPoints(String teamName) {
        int points = 0;
        if (this.getTeamHome().getName().equals(teamName)) {
            if (this.getScoreHome() > this.getScoreGuest()) {
                points = getWinnerPoints();
            } else if (this.getScoreHome() < this.getScoreGuest()) {
                points = getLoserPoints();
            } else {
                points = getNilPoints();
            }
        } else if (this.getTeamGuest().getName().equals(teamName)) {
            if (this.getScoreHome() < this.getScoreGuest()) {
                points = getWinnerPoints();
            } else if (this.getScoreHome() > this.getScoreGuest()) {
                points = getLoserPoints();
            } else {
                points = getNilPoints();
            }
        }

        //System.out.println("Points:"+points);
        return points;
    }

    public int getWinnerPoints() {
        return winnerPoints;
    }

    public void setWinnerPoints(int winnerPoints) {
        this.winnerPoints = winnerPoints;
    }

    public int getLoserPoints() {
        return loserPoints;
    }

    public void setLoserPoints(int loserPoints) {
        this.loserPoints = loserPoints;
    }

    public int getNilPoints() {
        return nilPoints;
    }

    public void setNilPoints(int nilPoints) {
        this.nilPoints = nilPoints;
    }
    

}
