/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mavrelos.eap.plh24.ge1.askhsh2;

import static java.lang.System.exit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *
 * @author mavrelos
 */
public abstract class Team /*implements Comparator<Team>, Comparable<Team>*/{

    /** Team name */
    private String name = "Team";
    
    /** Max Numbers of games */
    private int maxNumOfGames = 0; 
    
    /** Current number of games */
    protected int curNumOfGames = 0;
    
    /** Teem's Games array. A team can have up to two games */
    protected Game[] teamGames = new Game[3*3]; //οι αγώνες της ομάδας

    //constructor με τον οποίο εισάγονται το όνομα της ομάδας
    //και ο μέγιστος αριθμός παιχνιδιών
    public Team(String name, int maxNumOfGames){   
      //Init
      this.name = name;
      this.maxNumOfGames = maxNumOfGames;
    }
    
    //Accessors
    public void setName(String name){
        this.name=name;
    }
    
    public String getName(){
        return name;
    }         

    public int getMaxNumOfGames() {
        return maxNumOfGames;
    }

    public void setMaxNumOfGames(int maxNumOfGames) {
        this.maxNumOfGames = maxNumOfGames;
    }

    public Game[] getTeamGames() {
        return teamGames;
    }

    public void setTeamGames(Game[] teamGames) {
        this.teamGames = teamGames;
    }
           
    //We should throw some exceptions... not exit
    public void addGame(Game g){ 
        if (teamGames.length>9) {
            System.out.println("Cannot participate in more than 9 games. Exiting");
            exit(1);
        } else {
            this.teamGames[this.curNumOfGames++] = g;
        }
        if (g.getScoreGuest()<0 || g.getScoreHome()<0){
            System.out.println("Negative score. Exiting");
            exit(1);
        }
    }

    /**
     * Calculate how many points the team has so far
     * @return 
     */
    public int calcPoints(){
        //Use Game's getPoints() and sumup all points for this championship so fat
        return  Arrays
                .asList(teamGames)
                .stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList())
                .stream()
                .mapToInt(game->game.getPoints(getName()))
                .sum();
    }
    
    @Override
    public String toString() {
        return "Team:" + name + " Points="+calcPoints()+"\n";
    }
}