/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mavrelos.eap.plh24.ge1.askhsh2;

import com.mavrelos.eap.plh24.ge1.askhsh2.Championship.GameTp;
import static java.lang.System.exit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mavrelos
 */
public class Championship {

    static enum GameTp {
        FOOTBALL, BASKETBALL
    };
    static List<String> footballTeamNamesList = Arrays.asList("OLYMPIAKOS FC", "AEK FC", "PAO FC");
    static List<String> basketballTeamNamesList = Arrays.asList("PAO BC", "PAOK BC", "ARIS BC");

    static List<FootballTeam> footballTeamsList = new ArrayList<FootballTeam>();
    static List<BasketballTeam> basketballTeamsList = new ArrayList<BasketballTeam>();

    /**
     * Games with other teams and this team plays at home 
     * Key : X Team Name
     * Value : List of team names that X Team already played as host
     */
    static Map<String, List<String>> homeGamesMap = new HashMap<>();

    /**
     * Games with other teams and this team plays as guest 
     * Key : X Team Name
     * Value : List of team names that X Team already played as guest
     */
    static Map<String, List<String>> guestGamesMap = new HashMap<>();

    /**
     * Calc and Print the champion team
     * @param team 
     */
    static void calcChampion(List<? extends Team> team) {
        //Sort the teams by their points and peek the first one
        if (team instanceof FootballTeam) {
            System.out.print("Football champion:");
        } else {
            System.out.print("Basketball champion:");
        }
        team.sort(Comparator.comparing(Team::calcPoints, Comparator.reverseOrder()));
        System.out.println(team.get(0).getName());
    }

    /**
     * printTeamsInfo
     * @param team 
     */
    static void printTeamsInfo(List<? extends Team> team) {
        //if (team instanceof FootballTeam)
        System.out.println("----------------------- Teams Info -------------------------------");
        System.out.println(team.toString());
    }
    /**
     * Prints League order
     * @param team 
     */
    static void printLeague(List<? extends Team> team) {
        //Since teams list is already sorted from calcCampion()
        //we only need to print the names of the teams
        System.out.println("\n-----------------------League-------------------------------");
        team
                .stream()
                .forEach(aTeam -> System.out.println(aTeam.getName()));
    }

    /**
     * Construct a game means:
       1.Find a home team 
       2.Find another team that didnt played before as guest and register it as guest team
       Assign random score to each of them
       Basketball games will have a multiplier due to higher scores
     * @param teamsList
     * @param gameTp 
     */
    static void createAGame(List<? extends Team> teamsList, GameTp gameTp) {
        Team homeTeam = null; //Not a good practice
        Team guestTeam = null;
    //HOME
        //Use a temporary list to store the teams (names) that can play as host
        //We can do this by searching the homeGamesMap for a home team with incomplete guest list
        List<String> teamsWithPendingHomeGamesList = new ArrayList<>();
        for ( Map.Entry<String, List<String>> entry : homeGamesMap.entrySet()){
            if (entry.getValue().size() != teamsList.size()-1){
                teamsWithPendingHomeGamesList.add(entry.getKey());
            }
        }
        
        //Get a random one 
        int x = (int) (Math.random() * teamsWithPendingHomeGamesList.size());
        homeTeam = teamsList
                .stream()
                .filter(team -> team.getName().equals(teamsWithPendingHomeGamesList.get(x)))
                .findFirst()
                .get();
    //GUEST
        //Get a random team but not the hometeam again!
        //And do not choose team already played with
        final String homeTeamName = homeTeam.getName();
        List<String> teamsWithPendingGuestGamesList = new ArrayList<>();
        for (Map.Entry<String, List<String>> entry : guestGamesMap.entrySet()) {
            if ((!entry.getKey().equals(homeTeamName))
                    && (!entry.getValue().contains(homeTeamName))
                    && (entry.getValue().size() != teamsList.size() - 1)) {
                teamsWithPendingGuestGamesList.add(entry.getKey());
            }
        }
        
        int y = (int) (Math.random() * teamsWithPendingGuestGamesList.size());
        guestTeam = teamsList
                .stream()
                .filter(team -> team.getName().equals(teamsWithPendingGuestGamesList.get(y)))
                .findFirst()
                .get();

        homeGamesMap.get(homeTeam.getName()).add(guestTeam.getName());
        guestGamesMap.get(guestTeam.getName()).add(homeTeam.getName());

        //setup winning points
        //Again. This is BAD design called "workaround".
        //Points is a championship property
        //We sould have Abstract Championship class
        int multiplier = 1;
        int winningPoints = 0;
        int loserPoints = 0;
        int nilPoints = 0;

        switch (gameTp) {
            case FOOTBALL:
                winningPoints = 3;
                loserPoints = 0;
                nilPoints = 1;
                break;
            case BASKETBALL:
                winningPoints = 2;
                loserPoints = 1;
                multiplier = 10;
                break;
            default:
                System.out.println("Shouldn't be here");
        }

        //Assign some random scores
        int range = (10 - 0) + 1;     //0 to 10 goals
        int homeScore = (int) (Math.random() * range) + 0; //0 is min
        int guestScore = (int) (Math.random() * range) + 0;
        Game game = new Game(homeTeam, guestTeam, homeScore * multiplier, guestScore * multiplier, winningPoints, loserPoints, nilPoints);

        String guestTeamName = guestTeam.getName();

        homeTeam.addGame(game);
        guestTeam.addGame(game);

        System.out.println(homeTeamName + "-" + guestTeamName + ":" + game.getScoreHome() + "-" + game.getScoreGuest());
    }

    /**
     * Check if every team conclude its games as home and guest.
     * Actually we don't need to check guest list
     *
     * @return
     */
    static private boolean championshipIsFinished(List<? extends Team> teamsList) {
        //homeGamesMap is a Map with 
        //    key   = a team playing as home
        //    value = a list of team names that the home team already played with
        //By checking if every home team key has already two elements in it's value, we are sure that the championship is finished
        //And we can remove hardcoded (two) with counting all teams minus the one in key
        if (!homeGamesMap.entrySet()
                .stream()
                .noneMatch((entry) -> (entry.getValue().size() != teamsList.size()-1))) {
            return false;
        };
        return true;
    }

    /**
     * Create a match between two teams and assignes a random score
     * @param gameTp
     * @param teamsList 
     */
    static private void createMatchesAndOutcome(GameTp gameTp, List<? extends Team> teamsList) {
        /**
         * used for multiply the score by 10 for basketball games 1 = football
         * 10 = basketball
         */
        int multiplier = 1;

        //List<Game> gamesList = new ArrayList<Game>();
        System.out.println("Game Results");

        int gamesCount = 1;
        while (!championshipIsFinished(teamsList)) {
            System.out.print((gamesCount++) + ". ");
            createAGame(teamsList, gameTp);
        };
    }

    static void createTeams(GameTp gameTp) {
        //3 teams per championship with 2 games each
        switch (gameTp) {
            case FOOTBALL:
                footballTeamNamesList.forEach(name -> footballTeamsList.add(new FootballTeam(name, 4)));
                break;
            case BASKETBALL:
                basketballTeamNamesList.forEach(name -> basketballTeamsList.add(new BasketballTeam(name, 4)));
                break;
            default:
                System.out.println("Game " + gameTp + " not supported");
        }
    }

    //Housekeeping
    static private void initChampionship(GameTp gameTp) {
        homeGamesMap.clear();
        guestGamesMap.clear();

        switch (gameTp) {
            case FOOTBALL:
                footballTeamNamesList.forEach(aName -> {
                    homeGamesMap.put(aName, new ArrayList<>());
                    guestGamesMap.put(aName, new ArrayList<>());
                });
                break;
            case BASKETBALL:
                basketballTeamNamesList.forEach(aName -> {
                    homeGamesMap.put(aName, new ArrayList<>());
                    guestGamesMap.put(aName, new ArrayList<>());
                });
                break;
            default:
                System.out.println("wont get here");
                exit(1);
        }

    }

    //Scenarios
    static private void runChampionship(GameTp gameTp) {
        //Init Championship
        initChampionship(gameTp);

        System.out.println("=====================" + gameTp.toString() + "========================");

        //This is a BAD design, and this is a "workaround"
        //Championship should be abstract......
        //Since i need to write everything once i will point a reference to 
        //the needed list (footballteams or basketballteams)
        //Ofcourse referenceToATeam could be a class property but... fewer the better
        List<? extends Team> referenceToATeam = footballTeamsList;   //always init with something

        switch (gameTp) {
            case FOOTBALL:
                referenceToATeam = footballTeamsList;
                break;
            case BASKETBALL:
                referenceToATeam = basketballTeamsList;
                break;
            default:
                System.out.println("wont get here. Only football and basketball");
                exit(1);
        }

        createTeams(gameTp);
        createMatchesAndOutcome(gameTp, referenceToATeam);
        calcChampion(referenceToATeam);
        printTeamsInfo(referenceToATeam);
        printLeague(referenceToATeam);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        runChampionship(GameTp.FOOTBALL);
        runChampionship(GameTp.BASKETBALL);
    }
}